import sys
import os
import glob
import difflib
import json
from pprint import pprint

outputFile = "experiment/keywordresults.txt"

questionsResult = [
    {
        "question": "What color?",
        "options": {
            "red": 0,
            "white": 0,
            "green": 0,
            "purple": 0,
            "blue": 0,
            "yellow": 0
        }
    },
    {
        "question": "Which robot?",
        "options": {
            "alice": 0,
            "pseudo": 0
        }
    },
    {
        "question": "What location?",
        "options": {
            "table one": 0,
            "table two": 0,
            "table three": 0,
            "object shelve one": 0,
            "object shelve two": 0,
            "kitchen": 0,
            "bedroom": 0,
        }
    },
    {
        "question": "Which object?",
        "options": {
            "orange juice": 0,
            "ice tea": 0,
            "coffee": 0,
            "beer": 0,
            "chocolates": 0,
            "chewing gums": 0,
            "peanuts": 0,
        }
    },
]

with open(outputFile, "r") as f:
	outputContent = f.read().splitlines()

for run in outputContent:
	runData = json.loads(run)
	for runQuestion in runData:
		question = runQuestion['question']
		answers = runQuestion['answers']
		for answer in answers:
			if answers[answer] == True:
				for idx, questionResult in enumerate(questionsResult):
					if questionResult['question'] == question:
						questionsResult[idx]['options'][answer] += 1

pprint(questionsResult)

score = 0
numberOfOptions = 0
for questionResult in questionsResult:
	for option in questionResult['options']:
		numberOfOptions += 1
		score += questionResult['options'][option]

numberOfRuns = len(outputContent)
print ("Number of runs: %s" % numberOfRuns)
print ("Number of options: %s" % numberOfOptions)
print ("Total score: %s" % score)
print ("Average score: %f" % (float(score)/numberOfRuns))
print ("Score percentage: %f" % ((float(score)/numberOfRuns)/numberOfOptions))


