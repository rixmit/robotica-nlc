#!/bin/bash
FOLDER='experiment/recog_output/experiment2_slow/'
WAV='experiment/recordings/slow/'
echo $FOLDER
mkdir $FOLDER
python Wavrecognizerexperiment.py --gram grammar/experiment.gram --wav $WAV/0lab.wav --number 0
python Wavrecognizerexperiment.py --gram grammar/experiment.gram --wav $WAV/1lab.wav --number 1
python Wavrecognizerexperiment.py --gram grammar/experiment.gram --wav $WAV/2lab.wav --number 2
python Wavrecognizerexperiment.py --gram grammar/experiment.gram --wav $WAV/3lab.wav --number 3
python Wavrecognizerexperiment.py --gram grammar/experiment.gram --wav $WAV/4lab.wav --number 4
python Wavrecognizerexperiment.py --gram grammar/experiment.gram --wav $WAV/5lab.wav --number 5
python Wavrecognizerexperiment.py --gram grammar/experiment.gram --wav $WAV/6lab.wav --number 6
python Wavrecognizerexperiment.py --gram grammar/experiment.gram --wav $WAV/7lab.wav --number 7
python Wavrecognizerexperiment.py --gram grammar/experiment.gram --wav $WAV/8lab.wav --number 8
python Wavrecognizerexperiment.py --gram grammar/experiment.gram --wav $WAV/9lab.wav --number 9
