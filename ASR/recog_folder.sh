#!/bin/bash
FOLDER='experiment/recog_output/slow_1/'
WAV='experiment/recordings/slow/'
echo $FOLDER
mkdir $FOLDER
python Wavrecognizer.py --gram grammar/experiment.gram --wav $WAV/0lab.wav > $FOLDER/0.rec
python Wavrecognizer.py --gram grammar/experiment.gram --wav $WAV/1lab.wav > $FOLDER/1.rec
python Wavrecognizer.py --gram grammar/experiment.gram --wav $WAV/2lab.wav > $FOLDER/2.rec
python Wavrecognizer.py --gram grammar/experiment.gram --wav $WAV/3lab.wav > $FOLDER/3.rec
python Wavrecognizer.py --gram grammar/experiment.gram --wav $WAV/4lab.wav > $FOLDER/4.rec
python Wavrecognizer.py --gram grammar/experiment.gram --wav $WAV/5lab.wav > $FOLDER/5.rec
python Wavrecognizer.py --gram grammar/experiment.gram --wav $WAV/6lab.wav > $FOLDER/6.rec
python Wavrecognizer.py --gram grammar/experiment.gram --wav $WAV/7lab.wav > $FOLDER/7.rec
python Wavrecognizer.py --gram grammar/experiment.gram --wav $WAV/8lab.wav > $FOLDER/8.rec
python Wavrecognizer.py --gram grammar/experiment.gram --wav $WAV/9lab.wav > $FOLDER/9.rec
