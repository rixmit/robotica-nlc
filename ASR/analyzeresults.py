import sys
import os
import glob
import difflib
import json
from pprint import pprint

from DecoderAnalyzer import DecoderAnalyzer
from Conversator import Conversator

def compare(recognition, labeltext, nline):
	ndif = 0
	#print file2
	s = difflib.SequenceMatcher(None, recognition.split(), labeltext.split())
	#print s.ratio()

	q = difflib.ndiff(recognition.split(), labeltext.split())
	for a, b in enumerate(q):
		if b[0]=='+':
			ndif += 1
			print(b)
	print('#lines:', nline, "\t#errors:", ndif)
	return s.ratio(), ndif, nline

decoderAnalyzer = DecoderAnalyzer("grammar/experiment.gram")
conversator = Conversator("grammar/experiment.gram")

outputFolder = "experiment/recog_output/experiment2_slow_backup/"
correctFolder = "experiment/subsets/"

outputFiles = glob.glob("%s*.rec" % outputFolder)
correctFiles = glob.glob("%ssample_*.lbl" % correctFolder)

pairs = zip(sorted(outputFiles), sorted(correctFiles))


meval = 0 # number of sentence correctly labeled as correct understood or falsely understood
me = 0  # the mean number of inserted/removed characters for each file
mr = 0  # the mean ratio of agreement for each file
ml = 0  # the mean of detected sentences per file
count = 0
for pair in pairs:
	with open(pair[0], "r") as f:
		outputContent = f.read().splitlines()
	with open(pair[1], "r") as f:
		correctContent = f.read().splitlines()

	subPairs = zip(outputContent, correctContent)

	numberOfSubpairs = len(subPairs)
	recognition = ''
	labeltext = ''
	correctLines = 0
	correctEvaluations = 0
	for subPair in subPairs:
		output = json.loads(subPair[0])
		correct = subPair[1]
		# outputFiltered = decoderAnalyzer.filterHypstr(output['result'])
		correctFiltered = decoderAnalyzer.filterHypstr(correct)
		outputFiltered = output['analyzerResult']['first']['sentence']
		# outputFiltered = output['result']
		conversatorResult = conversator.resultResponse(output['analyzerResult'])
		# correctFiltered = correct
		# pprint(outputFiltered)
		# pprint(correctFiltered)
		if outputFiltered == correctFiltered:
			if conversatorResult['repeat'] == "no":
				# correctEvaluations += 1
				pass
			correctLines += 1
		else:
			if conversatorResult['repeat'] != "no":
				correctEvaluations += 1
		recognition += '\n' + outputFiltered
		labeltext += '\n' + correctFiltered

	[a,b,c] = compare(recognition, labeltext, numberOfSubpairs)
	me += float(a)/numberOfSubpairs
	mr += float(b)/numberOfSubpairs
	ml += float(correctLines)/numberOfSubpairs
	meval += float(correctEvaluations)/(numberOfSubpairs - correctLines)

	if count == 2:
		pass
	count += 1
print ("\nword recognition rate:\t\t %f \nmean #wrong words per sentence:\t\t %f \nmean recognized sentences:\t %f \nmean correct evaluations:\t %f" % (me, mr, ml, meval))
