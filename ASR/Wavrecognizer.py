import socket
import sys
import struct
import math
import argparse
import pyaudio
import subprocess
import ossaudiodev
import time
import threading
from pocketsphinx import *
from sphinxbase import *

hmm= 'model/en-us'
lm = 'model/en-us.lm'
dic = 'model/cmu07a.dic'

HOST = 'localhost'
PORT = 5530

parser = argparse.ArgumentParser(description='Borg Speech Recognition')
parser.add_argument("--gram")
parser.add_argument("--wav", default = None)
args = parser.parse_args()

# sphinx config
config = Decoder.default_config()
config.set_string('-logfn', '/dev/null') #comment for debug information
config.set_string('-hmm', hmm)
config.set_string('-lm', lm)
config.set_string('-dict', dic)
#config.set_string('-fsgusefiller', 'no') #removes filler words
#config.set_string('-rawlogdir', 'logs') #uncomment to log the raw stream data
print "[Wavrecognizer] loading language model ..."
decoder = Decoder(config)
print "[Wavrecognizer] done."

if not args.gram == None:
    jsgf = Jsgf(args.gram)
    rule = jsgf.get_rule('<vcc.all>')
    fsg = jsgf.build_fsg(rule, decoder.get_logmath(), 7.5)
    #fsg.writefile('grammar/vcc.fsg')
    decoder.set_fsg("newgram", fsg)
    decoder.set_search("newgram")
    print "switched to grammar",args.gram

if args.wav == None:
    print "please use this with a four channel wav file"
    exit(1)

simsrc = 3
debug = False
raw = [{'active': False, 'raw': '', 'ID': -1, 'azimuth': 0.0} for x in range(simsrc)]

# define decoding structures
header = struct.Struct('3I 2q')
srcinfo = struct.Struct('I 4f')
numsrc = struct.Struct('I')
srcdata = struct.Struct('2I')

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
    s.bind((HOST, PORT))
except socket.error , msg:
    print '[SpeechRecognizer] Connection to Hark failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    sys.exit()

s.listen(1)

hark_process = subprocess.Popen(['xterm', '-e',"./hark-config/wav_network.n "+ args.wav],stderr=subprocess.STDOUT)    
time.sleep(1)    
if hark_process.poll() == 0:    
    print "[SpeechRecognizer] Hark crashed, check 'arecord -l' for an kinect usb on plughw1:0"
    sys.exit()
conn, addr = s.accept()
print '[SpeechRecognizer] Connected with Hark @ ' + addr[0] + ':' + str(addr[1])

while True:
    
    #receive header (a)    
    data = conn.recv(header.size)
    try:
        header_data = header.unpack(data)
    except:
        break
    
    #receive number of sound sources (g)        
    data = conn.recv(numsrc.size)
    numsrc_data = numsrc.unpack(data)
    
    #reset flags in database
    for dat_0 in raw:    
        dat_0['active'] = False
    
    if numsrc_data[0]>0:          
                
        #for each sound source
        for x in range(numsrc_data[0]):

            #receive source info (h)        
            data = conn.recv(srcinfo.size)
            srcinfo_data = srcinfo.unpack(data)

            #store the ID, set active, and store azimuth           
            raw[srcinfo_data[0]%simsrc]['ID'] = srcinfo_data[0]
            raw[srcinfo_data[0]%simsrc]['active'] = True
            raw[srcinfo_data[0]%simsrc]['azimuth'] = 180/math.pi*math.atan2(srcinfo_data[2],srcinfo_data[1])

            #receive source data (i)
            data = conn.recv(srcdata.size)
            srcdata_data = srcdata.unpack(data)

            #receive raw audio (j)
            raw[srcinfo_data[0]%simsrc]['raw'] += conn.recv(srcdata_data[1])
    
    
    
    # now the datapackage is read, check for finished transmissions.
    for dat in raw:    
        if (not dat['active']) and (dat['ID'] >= 0):
            if debug: print "\ndecoding", dat['ID'], len(dat['raw'])
            try:            
                decoder.start_utt('Hark')  # sphinx
                decoder.process_raw(dat['raw'],False,True)
                decoder.end_utt()
                result = decoder.hyp().hypstr # best hypothesis
                if False:
                    print('\nAzimuth: %.2f (deg)' % dat['azimuth']),
                    if dat['azimuth'] > 0: print '(left)' 
                    else: print '(right)'
                print 'Source',dat['ID'],'Result:', result
                obs['result'] = result
                obs['recogtime'] = time.time()
                obs['azimuth'] = dat['azimuth']
            except:
                # print "I couldn't decode source", dat['ID']
                pass
                
            raw[dat['ID']%simsrc]['raw'] = "" 
            if debug: print 'after reset tawlen', len(dat['raw'])
            raw[dat['ID']%simsrc]['ID'] = -1
            raw[dat['ID']%simsrc]['azimuth'] = 0.0
decoder.end_utt()
print("PocketSphinx terminated")
