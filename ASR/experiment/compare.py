import re
import sys
import difflib
lst = []
file1 =''
file2 =''

if len(sys.argv)< 2:
    print "please input the recognition and the labels"
    exit(1)

print "comparing", sys.argv[1], 'and', sys.argv[2]

#regex = re.compile(r'(?:[Resu]) .*')
regex = re.compile(r'Result: (.*)')
with open(sys.argv[1]) as f:
    for line in f:
        #print line,
        try:
            res = regex.search(line)
            #print res.group(1)
            lst.append(res.group(1))
            file1 += '\n' + res.group(1)
        except:
            #print "no regex found"
            pass

with open(sys.argv[2]) as f:
    for line in f:
        #print line,
        file2 += line


s = difflib.SequenceMatcher(None, file1.split(),file2.split())
print s.ratio()

ndif = 0
q = difflib.ndiff(file1.split(),file2.split())

for a,b in enumerate(q):
    print b
    if b[0]==' ': continue
    ndif += 1

print ndif

t =difflib.unified_diff(file1,file2)
print t
