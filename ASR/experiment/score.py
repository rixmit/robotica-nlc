import re
import sys, os
import difflib
import glob

labeldir = 'subsets/'
recdir = sys.argv[1]
regex = re.compile(r'Result: (.*)')

reclist = glob.glob(recdir + '*.rec')
reclist.sort()

labellist = glob.glob(labeldir + '*.lbl')
labellist.sort()
#for item in reclist:
    #print item
#for item in labellist:
    # print item

def compare(rec, label):
    recognition = ''
    labeltext = ''
    ndif = 0
    nline = 0
    with open(rec) as f:
        for line in f:
            try:
                res = regex.search(line)
                recognition += '\n' + res.group(1)
                nline+=1
            except:
                #print "no regex found"
                pass

    with open(label) as f:
        for line in f:
            #print line,
            labeltext += line

    #print file2
    s = difflib.SequenceMatcher(None, recognition.split(), labeltext.split())
    #print s.ratio()

    q = difflib.ndiff(recognition.split(), labeltext.split())
    for a, b in enumerate(q):
        if b[0]=='+':
            ndif += 1
            print(b)
    print(rec, '   #lines:', nline, '\t#errors:', ndif)
    return s.ratio(), ndif, nline

me = 0  # the mean number of inserted/removed characters for each file
mr = 0  # the mean ratio of agreement for each file
ml = 0  # the mean of detected sentences per file
for i in range(0, 10):
    [a,b,c] = compare(reclist[i],labellist[i])
    me += float(a)/10
    mr += float(b)/10
    ml += float(c)/10
print ('\nword recognition rate:\t\t',me,'\nmean #wrong words:\t\t',mr,'\nmean recognized sentences:\t',ml)
