# vim: ft=make
.PHONY: nlcsmitvane._graphics
nlcsmitvane.aux nlcsmitvane.aux.make nlcsmitvane.d nlcsmitvane.pdf: $(call path-norm,/usr/share/texmf-texlive/tex/generic/babel/babel.sty)
nlcsmitvane.aux nlcsmitvane.aux.make nlcsmitvane.d nlcsmitvane.pdf: $(call path-norm,/usr/share/texmf-texlive/tex/latex/amsmath/amsbsy.sty)
nlcsmitvane.aux nlcsmitvane.aux.make nlcsmitvane.d nlcsmitvane.pdf: $(call path-norm,/usr/share/texmf-texlive/tex/latex/amsmath/amsgen.sty)
nlcsmitvane.aux nlcsmitvane.aux.make nlcsmitvane.d nlcsmitvane.pdf: $(call path-norm,/usr/share/texmf-texlive/tex/latex/amsmath/amsmath.sty)
nlcsmitvane.aux nlcsmitvane.aux.make nlcsmitvane.d nlcsmitvane.pdf: $(call path-norm,/usr/share/texmf-texlive/tex/latex/amsmath/amsopn.sty)
nlcsmitvane.aux nlcsmitvane.aux.make nlcsmitvane.d nlcsmitvane.pdf: $(call path-norm,/usr/share/texmf-texlive/tex/latex/amsmath/amstext.sty)
nlcsmitvane.aux nlcsmitvane.aux.make nlcsmitvane.d nlcsmitvane.pdf: $(call path-norm,/usr/share/texmf-texlive/tex/latex/base/article.cls)
nlcsmitvane.aux nlcsmitvane.aux.make nlcsmitvane.d nlcsmitvane.pdf: $(call path-norm,/usr/share/texmf-texlive/tex/latex/base/inputenc.sty)
nlcsmitvane.aux nlcsmitvane.aux.make nlcsmitvane.d nlcsmitvane.pdf: $(call path-norm,/usr/share/texmf-texlive/tex/latex/graphics/graphics.sty)
nlcsmitvane.aux nlcsmitvane.aux.make nlcsmitvane.d nlcsmitvane.pdf: $(call path-norm,/usr/share/texmf-texlive/tex/latex/graphics/graphicx.sty)
nlcsmitvane.aux nlcsmitvane.aux.make nlcsmitvane.d nlcsmitvane.pdf: $(call path-norm,/usr/share/texmf-texlive/tex/latex/graphics/keyval.sty)
nlcsmitvane.aux nlcsmitvane.aux.make nlcsmitvane.d nlcsmitvane.pdf: $(call path-norm,/usr/share/texmf-texlive/tex/latex/graphics/trig.sty)
nlcsmitvane.aux nlcsmitvane.aux.make nlcsmitvane.d nlcsmitvane.pdf: $(call path-norm,/usr/share/texmf-texlive/tex/latex/ucs/ucs.sty)
nlcsmitvane.aux nlcsmitvane.aux.make nlcsmitvane.d nlcsmitvane.pdf: $(call path-norm,nlcsmitvane.tex)
.SECONDEXPANSION:
-include pipeline.png.gpi.d
nlcsmitvane.d: $$(call graphics-source,pipeline.png)
nlcsmitvane.pdf nlcsmitvane._graphics: $$(call graphics-target,pipeline.png)
