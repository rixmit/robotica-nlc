\documentclass[12pt]{article}

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{}

\title{Natural Language Communication with PocketSphinx using n-best recognition scoring in Human-Robot Interaction}
\author{Tim van Elteren \& Rik Smit}

\begin{document}
\maketitle

\abstract{In this paper a method is proposed to improve natural language communication with PocketSphinx using n-best recognition scoring in human-robot interaction. Different approaches are compared to an existing implementation that only takes the best scoring recognized sentence in consideration. To perform scoring on n-best recognized sentences we introduce an extension to the JSGF grammar containing an extra keyword and parser for this keyword. The result is a system that can differentiate between clarification sentences based on the part of a sentence requiring clarification. Preliminary evaluation of the use of this system integrated in a domestic service robot shows promising results.}

\section{Introduction}
The research in this paper is focussed on improving an existing implementation and integration of the PocketSphinx speech recognition system integrated in a Human-Robot Interaction based dialogue system. The main goal of the research is to improve the recognition rate using a clarification system that results in questions aimed at missing or misunderstood parts of sentences. In turn this system is part of a behavior for a domestic service robot. The current system uses Hark and Sphinx for speech recognition\footnote{http://wiki.teamborg.nl/index.php/Speech\_Recognition}. The process pipeline is pictured in \ref{fig:pipeline}. 

\begin{figure}[!ht]
  \centering
  \includegraphics[width=\textwidth]{pipeline.png}
  \caption{Current pipeline for speech recognition.}
  \label{fig:pipeline}
\end{figure}

\section{State of the art}
The audio signal is captured by either a microphone or the Kinect. The Kinect has the advantage of having 4 internal microphones which helps reducing noise and improving localization. Hark processes all the sound signals whether it is a speech source or not. Using these sound signals, PocketSphinx is using a closed grammar to recognize the speech. The recognized speech text is send to memory so the behavior can read from this. The behavior is the part of the system that does the language communication with a person. Here decisions are made how the robot acts on the environment, for example by asking questions and giving answers to questions.

\section{Methods}
This section describes the methods used to perform n-best recognition scoring in the PocketSphinx system, and other approaches, to improve and differentiate the responses to the human in a dialogue to improve a more natural communication in Human-Robot Interaction.

\subsection{Current approach}
The speech recognition system uses PocketSphinx and allows us to use a dynamical changeable closed grammar for detecting speech. Questions or global behavior grammar can be used. The speech recognizer will return the best result or no result if it was not able to recognize any speech. The interaction with the environment is still primitive. No assumptions are made whether the recognized speech is correct.

\subsection{Project goals}
For this project the recognition system will be extended. The goal is to provide more natural and dynamic feedback and dialogue speech recognition system to the user in order to make the system (robot) act more natural with the environment and the persons in this environment. One of the main issues in interaction with an environment is to deal with uncertainties. Speech might not be recognized correctly or not recognized at all. Therefore in order to improve the current system we should know how we can detect when speech is not recognized correctly or not recognized at all.

\subsection{Problem decomposition}
The problem at hand and the required to be constructed behavior can be decomposed in the parts shown below. 

\begin{enumerate}
	\item Recognize speech.
    \item Find anomalies (poorly recognized text or non-recognized texts).
    \item Respond adequate to anomalies (e.g. reiterate question).
    \item Repeat until no more anomalies.
\end{enumerate}

Because the Hark system recognizes both speech and non-speech and we only want to improve the speech recognition itself, the focus in our research is mainly towards the PocketSphinx speech recognition system. Follow-up research consists of finding anomalies. These anomalies consist of poor or not recognized parts of sentences. The next step in this process is would be to determine in which part of the sentence the anomaly occured and trigger an adequate response, e.g. by reiterating the question with a focus to a specific part of the sentence that was poorly recognized. The information gathered in this process is used to improve the recognition of the whole sentence and afterwards the system has a greater confidence that it has recognized the sentence correctly.

\subsection{Approach}
Our approach is to derive a list of responses based on their likelihood and/or confidence. This list is used to determine if we want to ask the user a generic question, in the case of a low confidence score, and a specific question in the case of a high confidence score. The behavior will respond to the user and thereby creates a more human-like interaction. \cite{hazen02} describes a set of techniques for integrating confidence scores into the understanding and dialogue components of a speech understanding system. This will be our initial reference paper for our approach.

A possible extension to this system is to handle the interaction between two different persons in a different manner. By this we mean that we want to keep track of historic interaction with a person and differentiate the future responses to different persons based on the historic data. For example: person A usually asks for a coke. Another person informs the robot that he or she is out for a smoke. Due to the similarity in these words the robot might get confused. However using the historic data the robot can make an assumption about which person was talking to it and give a more adequate response. This can in turn be extended with the use of localization information provided by the Hark system. In this way the approaches can re-enforce each other in creating a more reliable human robot interaction system, as it can increase performance of the navigation and person recognition system in a sensor-fusion approach.
\subsection{Hypothesis}
We have a number of hypothesis to improve the recognition rate by using a behaviour that asks more information about a specific part of a sentence. To be able to determine which part of the sentence has been recognized poorly we need to gather information from PocketSphinx about the parts of sentences. 

\subsubsection{Scoring mechanisms}
There are a number of scoring mechanisms used by PocketSphinx to determine how well a part of the sentence has been recognized. Though we did manage to produce a score on the likelihood that we recognized the correct sentence, it was not obvious how this score was derived at and how it could be used to perform scoring on a subpart of a sentence. A typical approach is used by the PocketSphinx system itself to determine the likelihood that a word follows a preceding word. Unfortunately this system is not available through its application programming interface. Therefore we required to add another token and parser in order to recover the parts of the sentence.

\subsubsection{Parsing the grammar}
The grammar needs to be parsed in order to determine the parts to be indicated by the follow up question that the robot asks for more information from the user. This will be done by adding another tag in the grammar, that is ignored by the PocketSphinx grammar parser, in order to define the separation between recursive and non-recursive tags in the grammar. After processing the grammar using the extra tags we can determine which part of the sentence shows a low score in recognition. The extra token used is to recover recursion in the original grammar. These recursive tokens tyically 

\subsubsection{Best}
The typical and current approach is to use the best matching sentence as a whole and use that as the input for further actions. This does not consider the other possible sentences recognized or if parts of the sentence have a lower confidence determined by the system.

\subsubsection{n-best}
The n-best approach provides a list of sentences recognized and considers n number of sentences. The list by default is populated with starting with the top results down to the worst results. In the default case this provides a list of all the results. Because this list can be arbitrarily long, thus increasing the search space in case of another parsing step, we decided to narrow the search space by only considering a fixed number of the top n results; the n-best with an n of 10 and 2.

\subsubsection{Scores}
The scores that this approach provides have to be interpreted. They appear to be the log of the probability of the score. Though we are able to count the occurances of a similar sentence and therefore derive a top n-list of most probable sentences, the score is not very informative. Therefore we considered the following alternatives.

\subsubsection{10-best}
In this case we consider the 10 best results and compare the variations in score and sentences between the results of the scoring of the probability of a considered sentence versus the correct sentence. It proved that the search space in this case was very large and therefore slowed down the system as a whole. Therefore we considered an even narrower search space; only considering the best two results.

\subsubsection{2-best}
In this approach we only consider the 2-best in order to decrease the complexity of the scoring mechanism. We compare the score of the parts that the sentences consist of and perform this on the 2 best recognition results. After processing we determine the difference that we observe in the two sentences and determine which part, syntactically, this part is and determine which question corresponds to the missing information.

\subsubsection{Behaviors}
In order to improve the system we have studied a number of behaviours that are used by the robot for interaction with its environment are researched and the parts that are up for improvement are identified. Some example behaviours that we have studied, which can be found in the repository at \dots, are;
\begin{enumerate}
\item \verb|Src/behavior/borgdemo/borgdemo_1py (look at has_understood)|
\item \verb|src/behavior/examplequestion/examplequestion_1.py (look at ask function)|
\item \verb|Grammars at src/speech/hark-sphinx/grammar|
\end{enumerate}

\subsubsection{Preliminary findings}
The preliminary results that we found are the following;
\begin{enumerate}
\item The nbest list can be of arbitrary length.
\item The nbest list contains hypothesis sentences that are not valid according to the grammar.
\item Each nbest hypothesis has a score that gives an indication of how correct it is.
\item This score can not be trusted since we are using a closed grammar.
\item These score vary widely for different decodings.
\item The amount of occurrences of the same sentence gives an indication of how correct it is.
\end{enumerate}
These results confirm our hypothesis that with only the n-best results and the scores available directly from PocketSphinx are not informative enough to improve the recognition performance of the speech recognition and dialogue system. This is mainly due to the use of a closed grammar. Counting the number of occurrences of the same sentence provides an indication of the recognition result. Based on these preliminary findings we researched, implemented and tested two other approaches which are described in the following subsections.

\subsubsection{Method explained}
The method that we derived based on preliminary findings is to use the best 2 recognized sentences. These two sentences are analysed and determined in which part of the grammar the difference is located. In addition the grammar needs to be adjusted and an extra token needs to be added in all the places that involve recursion. This is because otherwise the subparts of the sentences can not be reversly parsed. The syntactic subpart of the sentence and grammar is used to determine the most appropriate question to gather the missing information. Otherwise the user is asked to repeat the whole question and the process is repeated. The example process is shown in the following enumeration:
%TODO add process diagram
\begin{enumerate}
\item Remove optional words
\item	These words are found by 'parsing' the grammar file
\item Take top 2 sentences.
\item	Use occurrence count to select top 2.
\item	If best hypothesis not in the two, take best hypothesis plus top 1
\item Find differences
\item	Using Python difflib
\item If one component different:
\item	Check if they are the same grammar variable.
\item	If yes:
\item		Check if it means the same thing (they have the same tag).
\item		If yes: no need to ask further. 
\item		If not: ask question specific to that component.
\item	If no:
\item		request repeat entire sentence
\item If completely different, request repeat entire sentence
\item	E.g: “I'm sorry I didn't understand what you were saying. Please repeat your sentence.”
\end{enumerate}
Some examples of how the process performs with two different sentences is described in the following subsection.
\subsubsection{Examples}
%TODO change to more graphic representation
\textbf{Example 1}
\begin{enumerate}
\item Found 2best:
\item alice follow me
\item pseudo follow me
\item Difference: alice vs pseudo
\item Word types:
\item alice $=> <start>$
\item pseudo $=> <start>$
\item Same Instance? $=>$ no (they don't have the same tags)
\item Rule:
\item If $<start>$ is uncertain then ask “Who do you mean?”
\end{enumerate}
\textbf{Example 2}
\begin{enumerate}
\item Found 2best:
\item alice take ice tea to kitchen
\item alice take peanuts to kitchen
\item Difference: ice tea vs peanuts
\item Word type:
\item ice tea $=> <object>$
\item peanuts $=> <object>$
\item Same Instance? $=>$ no (they don't have the same tags)
\item Rule:
\item if $<object>$ is uncertain then ask “What object do you mean?”
\item or more specific: “Do you mean 'ice tea' or 'peanuts'”,
\item Listen for objects
\end{enumerate}
%TODO: Nice list of references, but I couldn't find detailed info on Jupiter on how to download and work on it. It might be that (Pocket)Sphinx doesn't allow the same approach.

\section{Results and discussion}
The result of our implemented system is that we can differentiate between the responses based on the score of a recognized (sub-)set of sentences. We are able to determine based on the score and the extra information gathered which is the most appropriate response question. Using the information gathered from responses by the user to these questions we can improve the recognition results. Quantitative results are yet to be determined but the preliminary results show promise.

\begin{thebibliography}{99}
	\bibitem{bird09}
    	Bird, Steven, Ewan Klein, and Edward Loper. Natural language processing with Python. " O'Reilly Media, Inc.", 2009.
    \bibitem{hazen02}
    	Hazen, Timothy J., Stephanie Seneff, and Joseph Polifroni. "Recognition confidence scoring and its use in speech understanding systems." Computer Speech \& Language 16.1 (2002): 49-67.
    \bibitem{kemke06}
    	Kemke, Christel. "Natural language communication between human and artificial agents." Agent Computing and Multi-Agent Systems. Springer Berlin Heidelberg, 2006. 84-93.
\end{thebibliography}

\end{document}
